# RSA «Réseaux et Systèmes Avancés»

## Sommaire
- [Résumé du projet](#Résumé-du-projet)
- [Membres du groupe](#Membres-du-groupe)
- [Récupérer le projet](#Récupérer-le-projet)
- [Exécuter le projet](#Exécuter-le-projet)
- [Composition du répertoire](#Composition-du-répertoire)


## Résumé du projet

> MyP2P est un système d'échange de fichiers en mode pair à pair (P2P) composé:<br>
> - d'un serveur central qui stocke les métadonnées des fichiers, met en relation les pairs<br>
> - d'un pair qui agit à la fois comme client et serveur et réalise les publications, les recherches et les transferts de fichiers

Vous trouverez la description détaillée du projet dans le fichier [subject.pdf](./subject.pdf).


## Membres du groupe

- Florent Caspar <<florent.caspar@telecomnancy.eu>>
- Yann Colomb <<yann.colomb@telecomnancy.eu>>


## Récupérer le projet

Pour récupérer le projet, il vous suffit de faire :
- Par HTTPS :
```bash
$ git clone https://gitlab.telecomnancy.univ-lorraine.fr/Florent.Caspar/projet_rsa_caspar_colomb.git
```
- Par SSH :
```bash
$ git clone git@gitlab.telecomnancy.univ-lorraine.fr:Florent.Caspar/projet_rsa_caspar_colomb.git
```

## Exécuter le projet

### Compilation

La librairie *libssl-dev* est nécessaire pour compiler le projet. Pour l'installer:
```bash
$ sudo apt install libssl-dev -y
```


Il vous suffit ensuite de compiler le projet avec make:
```bash
$ make
```

Les exécutables *client* et *server* seront créés dans le dossier *out*.

### Utilisation

```
usage: ./out/server

Launch the MyP2P server.
```

```
usage: ./out/client <port>

Launch a MyP2P client.

positional arguments:
  port                   Client port (for both UDP/TCP)
```

## Composition du répertoire

### files

Le dossier [files](./files) contient les fichiers à partager (en tant que client).

### filesdl

Le dossier filesdl contient les fichiers téléchargés (en tant que client).

### Code source

Le dossier [src](./src) contient le code source de l'ensemble du projet.

### Options

Des options supplémentaires sont personnalisables dans [common.h](./src/common.h)
