.DEFAULT_GOAL := all
all: mrproper setup client server clean

srcDir := ./src
outDir := ./out
fileDir := ./files

client: client.o clientHelper.o common.o serializer.o
	gcc -g -pthread -o $(outDir)/client $(outDir)/client.o $(outDir)/clientHelper.o $(outDir)/common.o $(outDir)/serializer.o -lcrypto

server: server.o common.o serializer.o linkedSet.o
	gcc -g -pthread -o $(outDir)/server $(outDir)/server.o $(outDir)/common.o $(outDir)/serializer.o $(outDir)/linkedSet.o -lcrypto

client.o: $(srcDir)/client.c $(srcDir)/client.h
	gcc -g -o $(outDir)/client.o -c $(srcDir)/client.c -Wall -Wextra

clientHelper.o: $(srcDir)/clientHelper.c $(srcDir)/clientHelper.h
	gcc -g -o $(outDir)/clientHelper.o -c $(srcDir)/clientHelper.c -Wall -Wextra

server.o: $(srcDir)/server.c $(srcDir)/server.h
	gcc -g -o $(outDir)/server.o -c $(srcDir)/server.c -Wall -Wextra

common.o: $(srcDir)/common.c $(srcDir)/common.h
	gcc -g -o $(outDir)/common.o -c $(srcDir)/common.c -lcrypto -Wall -Wextra

linkedSet.o: $(srcDir)/linkedSet.c $(srcDir)/linkedSet.h
	gcc -g -o $(outDir)/linkedSet.o -c $(srcDir)/linkedSet.c -Wall -Wextra

serializer.o: $(srcDir)/structs.h $(srcDir)/serializer.c $(srcDir)/serializer.h
	gcc -g -o $(outDir)/serializer.o -c $(srcDir)/serializer.c -Wall -Wextra

clean:
	rm -rf $(outDir)/*.o

mrproper:
	rm -rf $(outDir)


setup:
	mkdir -p $(outDir)
	mkdir -p $(fileDir)
