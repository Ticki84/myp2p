#include "client.h"
#include "common.h"
#include <signal.h>
#include <unistd.h>
#include <sys/select.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>

void newfgets(char* buf, int bufsize, FILE* stream) {
    fgets(buf, bufsize, stream);
    buf[strcspn(buf, "\n")] = 0;
}

publish_t* input_publish() {
    printf("\nPlease enter your filename :\n");
    char filename[BUFSIZE/2];
    newfgets(filename, BUFSIZE/2, stdin);
    if (strcmp(filename, "") == 0) return NULL;
    printf("Please enter the extension of the file :\n");
    char extension[BUFSIZE/2];
    newfgets(extension, BUFSIZE/2, stdin);
    if (strcmp(extension, "") == 0) return NULL;
    printf("Please enter a CSV series of keywords defining your file (e.g. : cat,dog,animal) :\n");
    char* csv_keywords = malloc(sizeof(char)*BUFSIZE);
    newfgets(csv_keywords, BUFSIZE, stdin);
    if (strcmp(csv_keywords, "") == 0) return NULL;
    size_t keywords_size;
    char** keywords = csv_to_array(csv_keywords, &keywords_size);
    char completeFilename[BUFSIZE+8];
    snprintf(completeFilename, BUFSIZE+8 ,"./files/%s.%s",filename, extension);
    char* sha = sha1(completeFilename);
    if (sha == NULL)
    {
        return NULL;
    }
    return new_publish(filename, extension, (const char **) keywords, keywords_size, sha);
}

search_t* input_search() {
    printf("\nPlease enter a CSV series of keywords defining your search (e.g. : cat,dog,animal) :\n");
    char* csv_keywords = malloc(sizeof(char)*BUFSIZE);
    newfgets(csv_keywords, BUFSIZE, stdin);
    if (strcmp(csv_keywords, "") == 0) return NULL;
    size_t keywords_size;
    char** keywords = csv_to_array(csv_keywords, &keywords_size);
    return new_search((const char **) keywords, keywords_size);
}

int menu()
{
    int choice = -1;
    while (choice < 1) {
        printf("\nWhat do you want to do ?\n");
        printf("[1] Publish a file\n");
        printf("[2] Search a file\n");
        char readBuffer[3];
        newfgets(readBuffer, 3, stdin);
        if (strcoll(readBuffer, "") != 0) {
            choice = atoi(readBuffer);

            if (choice > 0 && choice <= 2) break;

            printf("Wrong choice. Please enter 1 or 2.\n");
            choice = -1;
        }
    }
    unsigned char* out_buffer = malloc(sizeof(char)*BUFSIZE);
    size_t out_buffer_len = 0;
    switch (choice) {
        case 1:
        {
            publish_t* pub = input_publish();
            if (pub == NULL) {
                printf("Publish aborted, returning to menu\n");
                return menu();
            }
            unsigned char* ptr = serialize_message(out_buffer, PUBLISH, pub);
            free_publish(pub);
            out_buffer_len = ptr - out_buffer;

            choice = PUBLISH;
            break;
        }
        case 2:
        {
            search_t* search = input_search();
            if (search == NULL) {
                printf("Search aborted (empty input), returning to menu\n");
                return menu();
            }
            unsigned char* ptr = serialize_message(out_buffer, SEARCH, search);
            free_search(search);
            out_buffer_len = ptr - out_buffer;

            choice = SEARCH;
            break;
        }
        default:
            choice = -1;
    }
    if (out_buffer_len != 0) {
        if (sendto(udp_sockfd, out_buffer, out_buffer_len, 0, (struct sockaddr*)&serv_addr, sizeof(serv_addr)) <= 0) {
            choice = -1;
            perror("Error on sendto()\n");
        }
    }
    free(out_buffer);
    return choice;
}

int download_menu(search_resp_t* search_resp) {
    int choice = -1;
    while (choice < 0) {
        printf("\n");
        for (unsigned int i=0; i<search_resp->results_size; i++) {
            printf("[%u] %s.%s (%s) - %s\n", i+1, search_resp->results[i]->filename, search_resp->results[i]->extension,
                   search_resp->results[i]->ip_address, search_resp->results[i]->hash);
        }
        printf("Please select the file you want to download:\n");

        char readBuffer[4];
        newfgets(readBuffer, 4, stdin);
        if (strcmp(readBuffer, "") == 0) {
            printf("No file selected, aborting download\n");
            break;
        }
        else {
            choice = atoi(readBuffer) - 1;

            if (choice >= 0 && choice < (int) search_resp->results_size) break;

            printf("Wrong selection.\n");
            choice = -1;
        }
    }
    return choice;
}

int process(int sent_type)
{
    socklen_t serv_len = sizeof(serv_addr);
    unsigned char* in_buf = malloc(sizeof(unsigned char)*BUFSIZE);
    if (recvfrom(udp_sockfd, in_buf, BUFSIZE, MSG_DONTWAIT, (struct sockaddr*) &serv_addr, &serv_len) <= 0) {
        perror("Error on recvfrom()\n");
        return EXIT_FAILURE;
    }

    int ret = EXIT_SUCCESS;

    message_type type;
    unsigned char* ptr = deserialize_message(in_buf, &type);
    switch (type) {
        case PUBLISH_ACK:
        {
            if (sent_type == PUBLISH) {
                printf("File successfully published\n");
            } else {
                perror("Unexpected response type\n");
                ret = EXIT_FAILURE;
            }
            break;
        }
        case SEARCH_RESP:
        {
            if (sent_type == SEARCH) {
                search_resp_t* search_resp = malloc(sizeof(search_resp_t));
                ptr = deserialize_search_resp(ptr, search_resp);
                if (ptr != NULL) {
                    printf("%zu results found\n", search_resp->results_size);

                    int choice = download_menu(search_resp);

                    if (choice >= 0)
                    {
                        pthread_t thread;
                        thread_search_resp_t* argsThreads = malloc(sizeof(thread_search_resp_t));
                        argsThreads->choice = choice;
                        argsThreads->search_resp = search_resp;
                        if (pthread_create(&thread, NULL, connectTCP, argsThreads)) {
                            free(argsThreads);
                            
                        }

                    }
                } else {
                    ret = EXIT_FAILURE;
                    perror("Unhandled SEARCH_RESP\n");
                }
            } else {
                perror("Unexpected response type\n");
                ret = EXIT_FAILURE;
            }
            break;
        }
        case PUBLISH_NACK:
        case SEARCH_NACK:
        {
            if ((sent_type == PUBLISH && type == PUBLISH_NACK) ||
                (sent_type == SEARCH && type == SEARCH_NACK)) {
                nack_t* nack = malloc(sizeof(nack_t));
                ptr = deserialize_nack(ptr, nack);
                if (ptr != NULL) {
                    printf("%s\n", nack->error);
                } else {
                    ret = EXIT_FAILURE;
                    perror("Unhandled NACK\n");
                }
                free(nack);
            } else {
                ret = EXIT_FAILURE;
                perror("Unexpected response type\n");
            }
            break;
        }
        default:
            ret = EXIT_FAILURE;
            perror("Received unhandled message type from server\n");
    }

    free(in_buf);
    return ret;
}

void send_file(int sockfd, char* fileName){
    FILE* fileToSend = fopen(fileName, "rb");

    fseek(fileToSend, 0, SEEK_END);          // Jump to the end of the file
    unsigned int filelen = ftell(fileToSend);         // Get the current byte offset in the file
    rewind(fileToSend);                      // Jump back to the beginning of the file

    unsigned char* toSend = malloc(BUFSIZE * sizeof(unsigned char));
    unsigned char* toSend_ptr = serialize_message(toSend, GET_ACK, NULL);
    send(sockfd, toSend, toSend_ptr - toSend, 0);

    unsigned char* file_buffer = malloc(filelen * sizeof(unsigned char)); // Enough memory for the file
    fread(file_buffer, filelen, 1, fileToSend); // Read in the entire file
    fclose(fileToSend);

    usleep(10000); // needed to ensure the other client is reading

    unsigned int sentSize = 0;
    while (sentSize < filelen)
    {
        int sizeToCopy = sentSize + BUFSIZE > filelen ? filelen - sentSize : BUFSIZE;
        if (write(sockfd, file_buffer+sentSize, sizeToCopy) < 0)
        {
            perror("Error send_file()");
        }
        sentSize += sizeToCopy;
    }
    
}

void recv_file(int sockfd, thread_search_resp_t* thread_args)
{
    unsigned char* buffer = malloc(BUFSIZE * sizeof(unsigned char));
    if (recv(sockfd, buffer, BUFSIZE, 0) > 0) {
        message_type type;
        deserialize_message(buffer, &type);
        if (type == GET_ACK)
        {
            char fileToWriteInName[256];
            sprintf(fileToWriteInName, "./filesdl/%s.%s",
                    thread_args->search_resp->results[thread_args->choice]->filename,
                    thread_args->search_resp->results[thread_args->choice]->extension);
            FILE* file=fopen(fileToWriteInName,"wb");

            memset(buffer, 0, BUFSIZE * sizeof(unsigned char));
            ssize_t read_len = 0;
            while((read_len = read(sockfd, buffer, BUFSIZE)) > 0) {
                fwrite(buffer, read_len, 1, file);
                memset(buffer, 0, BUFSIZE * sizeof(unsigned char));
            }

            fclose(file);




        }
        else if (type == GET_NACK)
        {
            printf("The file does no longer exist (file : %s.%s with hash %s)\n", thread_args->search_resp->results[thread_args->choice]->filename,
                                                                                  thread_args->search_resp->results[thread_args->choice]->extension,
                                                                                  thread_args->search_resp->results[thread_args->choice]->hash);
        }
        else
        {
            printf("Unhandled message type received\n");
        }
    } else {
        perror("Error on recv (expected GET_ACK or GET_NACK)\n");
    }
}


void* processTCPfileExchange() {
    /**
     * Here we should receive a GET, and the send the asked file 
     * 
    */
    int* ret = malloc(sizeof(int));
    // Create socket
    tcp_sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (tcp_sockfd < 0) {
        perror("socket()");
        *ret = EXIT_FAILURE;
        pthread_exit(&ret);
    }

    // Set up addres
    struct sockaddr_in server_addr, client_addr;
    memset(&server_addr, 0, sizeof(server_addr));
    server_addr.sin_family = AF_INET;
    server_addr.sin_addr.s_addr = htonl(INADDR_ANY);
    server_addr.sin_port = htons(client_port);
    
    bind(tcp_sockfd, (struct sockaddr*)&server_addr, sizeof(server_addr));
    listen(tcp_sockfd, MAX_CLIENTS);

    fd_set read_fds;
    fd_set write_fds;
    fd_set except_fds;
    int maxfd1 = tcp_sockfd + 1;
    int dialogfd;
    unsigned char* buffer = malloc(sizeof(char*)*BUFSIZE);

    for (;;) {
        // We build fds each time because select() updates fd sets
        FD_ZERO(&read_fds);
        FD_SET(tcp_sockfd, &read_fds);
        FD_ZERO(&write_fds);
        FD_SET(tcp_sockfd, &write_fds);
        FD_ZERO(&except_fds);
        FD_SET(tcp_sockfd, &except_fds);

        int sel = select(maxfd1, &read_fds, &write_fds, &except_fds, NULL);
        
        if (sel > 0) {

        
            if (FD_ISSET(tcp_sockfd, &read_fds)) {
                int len = sizeof(client_addr);
                dialogfd = accept(tcp_sockfd, (struct sockaddr*)&client_addr, (socklen_t*)&len);
                
                bzero(buffer, sizeof(char)*BUFSIZE);
                recv(dialogfd, buffer, sizeof(char)*BUFSIZE, 0);
                // tread in coming message
                message_type type;
                unsigned char* ptr = deserialize_message(buffer, &type);
                unsigned char* resp = malloc(sizeof(unsigned char) * BUFSIZE);
                unsigned char* resp_ptr = NULL;
                if (type == GET)
                {
                    get_t* get = malloc(sizeof(get_t));
                    ptr = deserialize_get(ptr, get);
                    
                    if (ptr != NULL)
                    {
                        // Successfully deserialized
                        printf("A client asked for this file : %s.%s with hash %s\n", get->filename, get->extension, get->hash);
                        char* fileToSendName = malloc(sizeof(char)*(strlen(get->filename)+strlen(get->extension)+10));
                        sprintf(fileToSendName, "./files/%s.%s", get->filename, get->extension);
                        if(access(fileToSendName, F_OK) == 0) 
                        {   // If the file exists
                            // We send the file (part by part) through the TCP stream
                            send_file(dialogfd, fileToSendName);
                        }
                         else {
                            // file doesn't exist
                            printf("file not found \n");

                            nack_t* nack = new_nack("No file found");
                            resp_ptr = serialize_message(resp, GET_NACK, nack);
                            send(dialogfd, resp, resp_ptr-resp, 0);
                        }
                    }
                }
                close(dialogfd);
            }
            if (FD_ISSET(tcp_sockfd, &except_fds)) {
                perror("Server fd exception\n");
                exit(EXIT_FAILURE);
            }
            
        } else if (sel == 0) {
            // Timeout
            perror("Timed out, either the server doesn't respond in time or the packet has been lost\nPlease try again!\n");
        } else {
            perror("Unexpected return value on select()\n");
            exit(EXIT_FAILURE);
        }
    }

}

void test_sha1(thread_search_resp_t* thread_args) {
    char filename[BUFSIZE];
    sprintf(filename, "./filesdl/%s.%s",
                            thread_args->search_resp->results[thread_args->choice]->filename,
                            thread_args->search_resp->results[thread_args->choice]->extension);
    if (!areSHA1equals(filename, thread_args->search_resp->results[thread_args->choice]->hash))
    {
        printf("The file is corrupted !\n");

    }

}

void* connectTCP(void* args) {
    /**
    Send a GET request to the other client

    */
    thread_search_resp_t* thread_args = (thread_search_resp_t*) args;
    int* ret = malloc(sizeof(int));
    *ret = EXIT_SUCCESS;
    
    int tcp_serverClient_sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (tcp_serverClient_sockfd < 0) {
        perror("socket()");
        *ret = EXIT_FAILURE;
        pthread_exit(&ret);
    }

    // Set up addres
    struct sockaddr_in serverClient_addr;
    memset(&serverClient_addr, 0, sizeof(serverClient_addr));
    serverClient_addr.sin_family = AF_INET;
    serverClient_addr.sin_addr.s_addr = inet_addr(thread_args->search_resp->results[thread_args->choice]->ip_address);
    serverClient_addr.sin_port = htons(thread_args->search_resp->results[thread_args->choice]->port);

    if (connect(tcp_serverClient_sockfd, (struct sockaddr*)&serverClient_addr, sizeof(serverClient_addr)) != 0) {
        perror("connect() didn't work");
        *ret = EXIT_FAILURE;
        free_search_resp(thread_args->search_resp);
        pthread_exit(&ret);
    }

    printf("Connected to %s:%d.\n", thread_args->search_resp->results[thread_args->choice]->ip_address, thread_args->search_resp->results[thread_args->choice]->port);

    fd_set read_fds;
    fd_set except_fds;
    
    int maxfd1 = tcp_serverClient_sockfd + 1;

    // Send a GET 

    // Create the GET :
    unsigned char* toSend = malloc(sizeof(unsigned char) * BUFSIZE);
    unsigned char* toSend_ptr = NULL;
    get_t* get = new_get(thread_args->search_resp->results[thread_args->choice]->filename, 
                        thread_args->search_resp->results[thread_args->choice]->extension,
                        thread_args->search_resp->results[thread_args->choice]->hash);
    // Send it
    toSend_ptr = serialize_message(toSend, GET, get);
    int toSendLen = toSend_ptr - toSend;
    send(tcp_serverClient_sockfd, toSend, toSendLen, 0);

    // We build fds each time because select() updates fd sets
    FD_ZERO(&read_fds);
    FD_SET(tcp_serverClient_sockfd, &read_fds);
    FD_ZERO(&except_fds);
    FD_SET(tcp_serverClient_sockfd, &except_fds);
    int sel = select(maxfd1, &read_fds, NULL, &except_fds, NULL);
    if (sel > 0) {
        if (FD_ISSET(tcp_serverClient_sockfd, &read_fds)) {
            // Receive a GET_RESP and handle it
            recv_file(tcp_serverClient_sockfd, thread_args);

            test_sha1(thread_args);
        }

        if (FD_ISSET(tcp_serverClient_sockfd, &except_fds)) {
            perror("Server fd exception\n");
            *ret = EXIT_FAILURE;
            free_search_resp(thread_args->search_resp);
        }
    } else if (sel == 0) {
        // Timeout
        perror("Timed out, either the server doesn't respond in time or the packet has been lost\nPlease try again!\n");
        free_search_resp(thread_args->search_resp);
    } else {
        perror("Unexpected return value on select()\n");
        *ret = EXIT_FAILURE;
        free_search_resp(thread_args->search_resp);
    }
    // TODO free_search_resp here?
    pthread_exit(ret);

}



void on_signal(int sig)
{
    if (sig == SIGINT || sig == SIGPIPE) {
        exit(EXIT_SUCCESS);
    }
}

int main(int argc, char* argv[])
{
    if (argc != 2)
    {
        printf("usage : ./out/client <port>\n");
        exit(1);
    }
    client_port = atoi(argv[1]);

    // Setup sigaction() to handle properly SIGINT and SIGPIPE signals
    struct sigaction sa;
    sa.sa_handler = on_signal;
    if (sigaction(SIGINT, &sa, 0) != 0 || sigaction(SIGPIPE, &sa, 0) != 0) {
        perror("Can't setup sigaction()\n");
        exit(EXIT_FAILURE);
    }

    // We create necessary dir if they don't exist
    mkdir("files", 0777);
    mkdir("filesdl", 0777);

    // thread TCP : should receive a GET, then answer and send the file
    pthread_t thread;
    pthread_create(&thread, NULL, processTCPfileExchange, NULL);

    // Create Socket
    udp_sockfd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
    if (udp_sockfd < 0) {
        perror("Error on socket()\n");
        exit(EXIT_FAILURE);
    }

    // Bind
    bzero(&cli_addr, sizeof(cli_addr));
    cli_addr.sin_family = AF_INET;
    cli_addr.sin_addr.s_addr= htonl(INADDR_ANY);
    cli_addr.sin_port=htons(client_port); // outgoing port for packets
    if (bind(udp_sockfd, (struct sockaddr *)&cli_addr, sizeof(cli_addr)) < 0) {
        perror("Can't bind socket\n");
        exit(EXIT_FAILURE);
    }

    bzero(&serv_addr, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = inet_addr(SERVER_ADDR);
    serv_addr.sin_port = htons(SERVER_PORT); // incoming port for packets


    printf("\n"
           "███╗   ███╗██╗   ██╗██████╗ ██████╗ ██████╗          ██████╗██╗     ██╗███████╗███╗   ██╗████████╗\n"
           "████╗ ████║╚██╗ ██╔╝██╔══██╗╚════██╗██╔══██╗        ██╔════╝██║     ██║██╔════╝████╗  ██║╚══██╔══╝\n"
           "██╔████╔██║ ╚████╔╝ ██████╔╝ █████╔╝██████╔╝        ██║     ██║     ██║█████╗  ██╔██╗ ██║   ██║   \n"
           "██║╚██╔╝██║  ╚██╔╝  ██╔═══╝ ██╔═══╝ ██╔═══╝         ██║     ██║     ██║██╔══╝  ██║╚██╗██║   ██║   \n"
           "██║ ╚═╝ ██║   ██║   ██║     ███████╗██║             ╚██████╗███████╗██║███████╗██║ ╚████║   ██║   \n"
           "╚═╝     ╚═╝   ╚═╝   ╚═╝     ╚══════╝╚═╝              ╚═════╝╚══════╝╚═╝╚══════╝╚═╝  ╚═══╝   ╚═╝   \n\n");

    fd_set read_fds, except_fds;
    int maxfd1 = udp_sockfd+1; // udp_sockfd socket always will be greater then STDIN_FILENO

    while (1) {
        // We call the menu, which returns the type of message sent or -1
        int message_type = menu();
        if (message_type >= 0) {
            // We build fds each time because select() updates fd sets
            FD_ZERO(&read_fds);
            FD_SET(udp_sockfd, &read_fds);
            FD_ZERO(&except_fds);
            FD_SET(udp_sockfd, &except_fds);
            // We set a timeout on waiting the server response
            struct timeval timeout;
            timeout.tv_sec = 2;
            timeout.tv_usec = 0;

            // We wait for the server response
            int sel = select(maxfd1, &read_fds, NULL, &except_fds, &timeout);
            if (sel > 0) {
                if (FD_ISSET(udp_sockfd, &read_fds)) {
                    if (process(message_type) != 0)
                        exit(EXIT_FAILURE);
                }
                if (FD_ISSET(udp_sockfd, &except_fds)) {
                    perror("Server fd exception\n");
                    exit(EXIT_FAILURE);
                }
            } else if (sel == 0) {
                // Timeout
                perror("Timed out, either the server doesn't respond in time or the packet has been lost\nPlease try again!\n");
            } else {
                perror("Unexpected return value on select()\n");
                exit(EXIT_FAILURE);
            }
        }
    }

    return 0;
}