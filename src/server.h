#ifndef PROJET_RSA_CASPAR_COLOMB_SERVER_H
#define PROJET_RSA_CASPAR_COLOMB_SERVER_H

#include <malloc.h>
#include <pthread.h>
#include <arpa/inet.h>
#include "linkedSet.h"

int sockfd;
linked_set_t* head = NULL;
pthread_mutex_t mutex;

typedef struct {
    unsigned char* in_buf;
    struct sockaddr_in cli_addr;
} thread_args_t;

void* process(void* args);
void on_signal(int sig);

#endif //PROJET_RSA_CASPAR_COLOMB_SERVER_H
