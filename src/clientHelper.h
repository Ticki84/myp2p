#ifndef PROJET_RSA_CASPAR_COLOMB_CLIENTHELPER_H
#define PROJET_RSA_CASPAR_COLOMB_CLIENTHELPER_H

#include "serializer.h"

publish_t* new_publish(const char* filename, const char* extension, const char** keywords, size_t keywords_size, const char* hash);
search_t* new_search(const char** keywords, size_t keywords_size);
get_t* new_get(const char* filename, const char* extension, const char* hash);
void* free_get(get_t* get);

#endif //PROJET_RSA_CASPAR_COLOMB_CLIENTHELPER_H
