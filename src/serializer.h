#ifndef PROJET_RSA_CASPAR_COLOMB_SERIALIZER_H
#define PROJET_RSA_CASPAR_COLOMB_SERIALIZER_H

#include <string.h>
#include "structs.h"

unsigned char* serialize_message(unsigned char* buffer, message_type type, const void* message);
// serialize primitives
unsigned char* serialize_bool(unsigned char* buffer, const int value);
unsigned char* serialize_unsigned_short(unsigned char* buffer, const unsigned short value);
unsigned char* serialize_unsigned_int(unsigned char* buffer, const unsigned int value);
unsigned char* serialize_str(unsigned char* buffer, const char* value);
unsigned char* serialize_str_arr(unsigned char* buffer, const size_t arr_size, const char** value);
// serialize structs
unsigned char* serialize_publish(unsigned char* buffer, const publish_t* value);
unsigned char* serialize_search(unsigned char* buffer, const search_t* value);
unsigned char* serialize_search_res(unsigned char* buffer, const search_res_t* value);
unsigned char* serialize_search_resp(unsigned char* buffer, const search_resp_t* value);
unsigned char* serialize_get(unsigned char* buffer, const get_t* value);
unsigned char* serialize_get_resp(unsigned char* buffer, const get_resp_t* value);
unsigned char* serialize_nack(unsigned char* buffer, const nack_t* value);

unsigned char* deserialize_message(unsigned char* buffer, message_type* type);
// deserialize primitives
unsigned char* deserialize_bool(unsigned char* buffer, int* value);
unsigned char* deserialize_unsigned_short(unsigned char* buffer, unsigned short* value);
unsigned char* deserialize_unsigned_int(unsigned char* buffer, unsigned int* value);
unsigned char* deserialize_str(unsigned char* buffer, char** value);
unsigned char* deserialize_str_arr(unsigned char* buffer, size_t* arr_size, char*** value);
// deserialize structs
unsigned char* deserialize_publish(unsigned char* buffer, publish_t* value);
unsigned char* deserialize_search(unsigned char* buffer, search_t* value);
unsigned char* deserialize_search_res(unsigned char* buffer, search_res_t* value);
unsigned char* deserialize_search_resp(unsigned char* buffer, search_resp_t* value);
unsigned char* deserialize_get(unsigned char* buffer, get_t* value);
unsigned char* deserialize_get_resp(unsigned char* buffer, get_resp_t* value);
unsigned char* deserialize_nack(unsigned char* buffer, nack_t* value);

#endif //PROJET_RSA_CASPAR_COLOMB_SERIALIZER_H
