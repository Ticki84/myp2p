#ifndef PROJET_RSA_CASPAR_COLOMB_LINKEDSET_H
#define PROJET_RSA_CASPAR_COLOMB_LINKEDSET_H

#include "structs.h"

typedef struct linked_set_t {
    search_res_t * data;
    struct linked_set_t* next;
} linked_set_t;

linked_set_t* insert(linked_set_t* head, const char* ip, const unsigned short port, publish_t* file);
linked_set_t* insert_res(linked_set_t* head, search_res_t* res);
int contains(linked_set_t* head, search_res_t* res);
search_resp_t* searchs(linked_set_t* head, char** keywords, size_t keywords_size);
linked_set_t* free_linked_set(linked_set_t* head);
linked_set_t* deep_free_linked_set(linked_set_t* head);

#endif //PROJET_RSA_CASPAR_COLOMB_LINKEDSET_H
