#ifndef PROJET_RSA_CASPAR_COLOMB_STRUCTS_H
#define PROJET_RSA_CASPAR_COLOMB_STRUCTS_H

typedef enum { PUBLISH, PUBLISH_ACK, PUBLISH_NACK, SEARCH, SEARCH_RESP, SEARCH_NACK, GET, GET_ACK, GET_RESP, GET_NACK } message_type;

typedef struct
{
    char* filename;
    char* extension;
    char** keywords;
    size_t keywords_size;
    char* hash;
} publish_t;

typedef struct
{
    char** keywords;
    size_t keywords_size;
} search_t;

typedef struct
{
    char* ip_address;
    unsigned short port;
    char* filename;
    char* extension;
    char** keywords;
    size_t keywords_size;
    char* hash;
} search_res_t;

typedef struct
{
    search_res_t** results;
    size_t results_size;
} search_resp_t;

typedef struct
{
    char* filename;
    char* extension;
    char* hash;
} get_t;

typedef struct
{
    unsigned int file_size;
} get_resp_t;

typedef struct
{
    char* error;
} nack_t;

#endif //PROJET_RSA_CASPAR_COLOMB_STRUCTS_H
