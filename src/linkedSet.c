#include <malloc.h>
#include <string.h>
#include "linkedSet.h"
#include "common.h"

linked_set_t* insert(linked_set_t* head, const char* ip, const unsigned short port, publish_t* file)
{
    search_res_t* res = malloc(sizeof(search_res_t));
    res->ip_address = malloc(strlen(ip)+1);
    strcpy(res->ip_address, ip);
    res->port = port;
    res->filename = malloc(strlen(file->filename)+1);
    strcpy(res->filename, file->filename);
    res->extension = malloc(strlen(file->extension)+1);
    strcpy(res->extension, file->extension);
    res->keywords = malloc(file->keywords_size*sizeof(char*));
    for (unsigned int j=0; j < file->keywords_size; j++) {
        res->keywords[j] = malloc(strlen(file->keywords[j]) + 1);
        strcpy(res->keywords[j], file->keywords[j]);
    }
    res->keywords_size = file->keywords_size;
    res->hash = malloc(strlen(file->hash)+1);
    strcpy(res->hash, file->hash);
    return insert_res(head, res);
}

linked_set_t* insert_res(linked_set_t* head, search_res_t* res)
{
    if (contains(head, res)) return head;
    linked_set_t* insertNode = malloc(sizeof(linked_set_t));

    insertNode->next = head;
    insertNode->data = res;
    return insertNode;
}

int contains(linked_set_t* head, search_res_t* res) {
    linked_set_t* cur = head;
    if (cur != NULL) {
        do {
            if (cur->data == res)
                return 1;
            else if (strcoll(cur->data->ip_address, res->ip_address) == 0 &&
                     cur->data->port == res->port &&
                     strcoll(cur->data->filename, res->filename) == 0 &&
                     strcoll(cur->data->extension, res->extension) == 0 &&
                     cur->data->keywords_size == res->keywords_size &&
                     strcoll(cur->data->hash, res->hash) == 0) {
                for (unsigned int i=0; i<cur->data->keywords_size; i++) {
                    if (strcoll(cur->data->keywords[i], res->keywords[i]) != 0)
                        return 0;
                }
                return 1;
            }
        } while ((cur = cur->next) != NULL);
    }
    return 0;
}

search_resp_t* searchs(linked_set_t* head, char** keywords, size_t keywords_size) {
    search_resp_t* resp = malloc(sizeof(search_resp_t));
    resp->results = malloc(sizeof(search_res_t*)*SEARCH_RES_LIMIT);
    resp->results_size = 0;
    if (head != NULL) {
        linked_set_t* cur = head;
        linked_set_t* taken = NULL;
        do {
            if (resp->results_size >= SEARCH_RES_LIMIT) break;
            for (unsigned int i=0; i<keywords_size; i++) {
                for (unsigned int j=0; j<cur->data->keywords_size; j++) {
                    if (strcoll(cur->data->keywords[j], keywords[i]) == 0) {
                        if (!contains(taken, cur->data)) {
                            taken = insert_res(taken, cur->data);
                            resp->results[resp->results_size] = cur->data;
                            resp->results_size += 1;
                        }
                        break;
                    }
                }
            }
        } while ((cur = cur->next) != NULL);
        free_linked_set(taken);
    }
    return resp;
}

linked_set_t* free_linked_set(linked_set_t* head) {
    while (head != NULL) {
        linked_set_t* tmp = head;
        head = head->next;
        free(tmp);
    }
    return head;
}

linked_set_t* deep_free_linked_set(linked_set_t* head) {
    while (head != NULL) {
        free_search_res(head->data);
        linked_set_t* tmp = head;
        head = head->next;
        free(tmp);
    }
    return head;
}
