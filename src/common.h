#ifndef PROJET_RSA_CASPAR_COLOMB_COMMON__H
#define PROJET_RSA_CASPAR_COLOMB_COMMON__H

#include "serializer.h"

// Set the server address
#define SERVER_ADDR "127.0.0.1"
// Set the server port
#define SERVER_PORT 45267
// Set the maximum payload size for both UDP and TCP
#define BUFSIZE 1400
// Set the maximum number of results for a search
#define SEARCH_RES_LIMIT 16
// Set the maximum number of clients that can connect to get files
#define MAX_CLIENTS 10

char** csv_to_array(const char* csv, size_t* size);
void* free_publish(publish_t* publish);
void* free_search(search_t* search);
void* free_search_res(search_res_t* search_res);
void* free_search_resp(search_resp_t* search_resp);
nack_t* new_nack(const char* error);
void* free_nack(nack_t* nack);
int areSHA1equals(char* filename1, char* sha1file);
char* sha1(char* filename);


#endif //PROJET_RSA_CASPAR_COLOMB_COMMON__H
