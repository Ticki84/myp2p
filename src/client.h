#ifndef PROJET_RSA_CASPAR_COLOMB_CLIENT_H
#define PROJET_RSA_CASPAR_COLOMB_CLIENT_H

#include "clientHelper.h"
#include <netdb.h>
#include <stdio.h>
#include <arpa/inet.h>

int udp_sockfd;
int tcp_sockfd;
int client_port;
struct sockaddr_in serv_addr;
struct sockaddr_in cli_addr;

typedef struct {
    search_resp_t* search_resp;
    int choice;
} thread_search_resp_t;

void newfgets(char* buf, int bufsize, FILE* stream);
publish_t* input_publish();
search_t* input_search();
int menu();
int download_menu(search_resp_t* search_resp);
int process(int sent_type);
void on_signal(int sig);
void* processTCPfileExchange();
void* connectTCP(void* args);

#endif //PROJET_RSA_CASPAR_COLOMB_CLIENT_H
