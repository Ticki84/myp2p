#include <malloc.h>
#include <string.h>
#include <openssl/sha.h>
#include <stdio.h>
#include <stdlib.h>
#include "common.h"

char** csv_to_array(const char* csv, size_t* size) {
    char** array = malloc(32*sizeof(char*));
    *size = 0;
    char* keyword;
    char* rest = strdup(csv);
    while ((keyword = strtok_r(rest, ",", &rest))) {
        int found = 0;
        for (unsigned int i=0; i<*size; i++) {
            // Check that keyword isn't a duplicate
            if (strcoll(keyword, array[i]) == 0) {
                found = 1;
                break;
            }
        }
        if (!found) {
            array[*size] = malloc(strlen(keyword)+1);
            strcpy(array[*size], keyword);
            *size += 1;
        }
    }
    return array;
}

void* free_publish(publish_t* publish) {
    free(publish->filename);
    free(publish->extension);
    for (unsigned int i=0; i<publish->keywords_size; i++) {
        free(publish->keywords[i]);
    }
    free(publish->keywords);
    free(publish->hash);
    free(publish);
    return NULL;
}

void* free_search(search_t* search) {
    for (unsigned int i=0; i<search->keywords_size; i++) {
        free(search->keywords[i]);
    }
    free(search->keywords);
    free(search);
    return NULL;
}

void* free_search_res(search_res_t* search_res) {
    free(search_res->ip_address);
    free(search_res->filename);
    free(search_res->extension);
    for (unsigned int i=0; i<search_res->keywords_size; i++) {
        free(search_res->keywords[i]);
    }
    free(search_res->keywords);
    free(search_res->hash);
    return NULL;
}

void* free_search_resp(search_resp_t* search_resp) {
    for (unsigned int i=0; i<search_resp->results_size; i++) {
        free_search_res(search_resp->results[i]);
    }
    free(search_resp->results);
    free(search_resp);
    return NULL;
}

nack_t* new_nack(const char* error) {
    nack_t* res = malloc(sizeof(nack_t));
    res->error = malloc(strlen(error)+1);
    strcpy(res->error, error);
    return res;
}

void* free_nack(nack_t* nack) {
    free(nack->error);
    free(nack);
    return NULL;
}


char* sha1(char* filename)
{
    unsigned char hash[SHA_DIGEST_LENGTH];
    unsigned char* result = malloc(sizeof(unsigned char) * 2 * SHA_DIGEST_LENGTH);
    int i;
    FILE *f = fopen(filename,"rb");
    SHA_CTX mdContent;
    int bytes;
    unsigned char data[1024];

    if(f == NULL){
        printf("Couldn't open file %s\n",filename);
        return NULL;
    }

    SHA1_Init(&mdContent);
    while((bytes = fread(data, 1, 1024, f)) != 0){

        SHA1_Update(&mdContent, data, bytes);
    }

    SHA1_Final(hash,&mdContent);

    fclose(f);

    for(i=0; i < SHA_DIGEST_LENGTH;i++){
        sprintf((char *)&(result[i*2]), "%02x",hash[i]);
    }

    return (char*)result;
}

int areSHA1equals(char* filename1, char* sha1file) {
    if (!strcmp((char*)sha1(filename1),sha1file))
    {
        return 1;
    }
    return 0;
    
}


