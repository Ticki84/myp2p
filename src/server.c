#include "server.h"
#include "common.h"
#include <stdio.h>
#include <signal.h>
#include <stdlib.h>
#include <string.h>


void* process(void* args) {
    thread_args_t* thread_args = args;

    int* ret = malloc(sizeof(int));
    char err_msg[128];

    char *ip = inet_ntoa(thread_args->cli_addr.sin_addr);
    unsigned short int port = ntohs(thread_args->cli_addr.sin_port);
    message_type type;
    unsigned char* ptr = deserialize_message(thread_args->in_buf, &type);
    unsigned char* resp = malloc(sizeof(unsigned char) * BUFSIZE);
    unsigned char* resp_ptr = NULL;
    size_t resp_len;
    switch (type) {
        case PUBLISH:
        {
            publish_t* pub = malloc(sizeof(publish_t));
            ptr = deserialize_publish(ptr, pub);
            if (ptr != NULL) {
                // Successfully deserialized
                printf("%s -> PUBLISH { %s, %hu, %s, %s, { ", ip, ip, port, pub->filename, pub->extension);
                for (unsigned int i=0; i<pub->keywords_size; i++) {
                    printf("%s%s ", pub->keywords[i], i<pub->keywords_size-1?",":"");
                }
                printf("}, %s }\n", pub->hash);
                // Saving file's details on the server
                pthread_mutex_lock(&mutex);
                linked_set_t* tmp_head = insert(head, ip, port, pub);
                if (tmp_head != head) {
                    // File has been added
                    head = tmp_head; // we update files' set head
                    pthread_mutex_unlock(&mutex);
                    resp_ptr = serialize_message(resp, PUBLISH_ACK, NULL);
                    printf("%s <- PUBLISH_ACK { }\n", ip);
                } else {
                    // File already exists
                    pthread_mutex_unlock(&mutex);
                    *ret = EXIT_FAILURE;
                    strcpy(err_msg, "PUBLISH rejected because published file already exists");
                }
            } else {
                *ret = EXIT_FAILURE;
                fprintf(stderr, "%s -> UNHANDLED PUBLISH", ip);
                strcpy(err_msg, "Can't deserialize PUBLISH");
            }

            if (*ret == EXIT_FAILURE) {
                nack_t* nack = new_nack(err_msg);
                resp_ptr = serialize_message(resp, PUBLISH_NACK, nack);
                printf("%s <- PUBLISH_NACK { %s }\n", ip, err_msg);
                free_nack(nack);
            }

            free_publish(pub);
            break;
        }
        case SEARCH:
        {
            search_t* search = malloc(sizeof(search_t));
            ptr = deserialize_search(ptr, search);
            if (ptr != NULL) {
                printf("%s -> SEARCH { ", ip);
                for (unsigned int i = 0; i < search->keywords_size; i++) {
                    printf("%s%s ", search->keywords[i], i < search->keywords_size - 1 ? "," : "");
                }
                printf("}\n");
                search_resp_t* search_resp = searchs(head, search->keywords, search->keywords_size);
                if (search_resp->results_size > 0) {
                    resp_ptr = serialize_message(resp, SEARCH_RESP, search_resp);
                    printf("%s <- SEARCH_RESP { ", ip);
                    for (unsigned int i=0; i<search_resp->results_size; i++) {
                        printf("%s{ %s, ... } ", i==0?"":", ", search_resp->results[i]->filename);
                    }
                    printf("}\n");
                } else {
                    *ret = EXIT_FAILURE;
                    strcpy(err_msg, "No result");
                }
                free(search_resp);
            } else {
                *ret = EXIT_FAILURE;
                fprintf(stderr, "%s -> UNHANDLED SEARCH\n", ip);
                strcpy(err_msg, "Can't deserialize SEARCH");
            }
            free_search(search);

            if (*ret == EXIT_FAILURE) {
                nack_t* nack = new_nack(err_msg);
                resp_ptr = serialize_message(resp, SEARCH_NACK, nack);
                printf("%s <- SEARCH_NACK { %s }\n", ip, err_msg);
                free_nack(nack);
            }
            break;
        }
        default:
            *ret = EXIT_FAILURE;
            fprintf(stderr, "Received unhandled message type from %s\n", ip);
    }

    if (resp_ptr != NULL) {
        resp_len = resp_ptr - resp;
        socklen_t cli_len = sizeof(thread_args->cli_addr);
        if (sendto(sockfd, resp, resp_len, 0, (struct sockaddr *) &thread_args->cli_addr, cli_len) <= 0) {
            perror("Error on sendto()\n");
            *ret = EXIT_FAILURE;
        }
    }

    free(resp);
    free(thread_args->in_buf);
    free(thread_args);
    pthread_exit(&ret);
}

void on_signal(int sig)
{
    if (sig == SIGINT || sig == SIGPIPE) {
        pthread_mutex_lock(&mutex);
        deep_free_linked_set(head);
        pthread_mutex_unlock(&mutex);
        exit(EXIT_SUCCESS);
    }
}

int main() {
    // Setup sigaction() to handle properly SIGINT and SIGPIPE signals
    struct sigaction sa;
    sa.sa_handler = on_signal;
    if (sigaction(SIGINT, &sa, 0) != 0 || sigaction(SIGPIPE, &sa, 0) != 0) {
        perror("Can't setup sigaction()\n");
        exit(EXIT_FAILURE);
    }

    // Create Socket
    sockfd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
    if (sockfd < 0) {
        perror("Error on socket()\n");
        exit(EXIT_FAILURE);
    }

    // Bind
    struct sockaddr_in serv_addr;
    bzero(&serv_addr, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = htonl(INADDR_ANY);
    serv_addr.sin_port = htons(SERVER_PORT); // outgoing port for packets
    if (bind(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0) {
        perror("Can't bind socket\n");
        exit(EXIT_FAILURE);
    }

    // Mutex init
    pthread_mutex_init(&mutex, NULL);


    printf("\n"
           "███╗   ███╗██╗   ██╗██████╗ ██████╗ ██████╗         ███████╗███████╗██████╗ ██╗   ██╗███████╗██████╗ \n"
           "████╗ ████║╚██╗ ██╔╝██╔══██╗╚════██╗██╔══██╗        ██╔════╝██╔════╝██╔══██╗██║   ██║██╔════╝██╔══██╗\n"
           "██╔████╔██║ ╚████╔╝ ██████╔╝ █████╔╝██████╔╝        ███████╗█████╗  ██████╔╝██║   ██║█████╗  ██████╔╝\n"
           "██║╚██╔╝██║  ╚██╔╝  ██╔═══╝ ██╔═══╝ ██╔═══╝         ╚════██║██╔══╝  ██╔══██╗╚██╗ ██╔╝██╔══╝  ██╔══██╗\n"
           "██║ ╚═╝ ██║   ██║   ██║     ███████╗██║             ███████║███████╗██║  ██║ ╚████╔╝ ███████╗██║  ██║\n"
           "╚═╝     ╚═╝   ╚═╝   ╚═╝     ╚══════╝╚═╝             ╚══════╝╚══════╝╚═╝  ╚═╝  ╚═══╝  ╚══════╝╚═╝  ╚═╝\n\n");
    printf("\nWaiting for connection...\n");
    while (1) {
        unsigned char *in_buf = malloc(sizeof(unsigned char) * BUFSIZE);
        bzero(in_buf, BUFSIZE);
        struct sockaddr_in cli_addr;
        bzero(&cli_addr, sizeof(cli_addr));
        cli_addr.sin_family = AF_INET;
        cli_addr.sin_addr.s_addr = htonl(INADDR_ANY);
        socklen_t cli_len = sizeof(cli_addr);
        ssize_t bytes_read = recvfrom(sockfd, in_buf, BUFSIZE, 0,
                                      (struct sockaddr *) &cli_addr, &cli_len);

        if (bytes_read > 0) {
            pthread_t thread;
            thread_args_t *args = malloc(sizeof *args);
            args->cli_addr = cli_addr;
            args->in_buf = in_buf;
            if (pthread_create(&thread, NULL, process, args)) {
                free(args);
            }
        } else {
            perror("Error on recvfrom()\n");
            exit(EXIT_FAILURE);
        }
    }

    return 0;
}
