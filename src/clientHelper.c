#include <malloc.h>
#include "clientHelper.h"

publish_t* new_publish(const char* filename, const char* extension, const char** keywords, const size_t keywords_size, const char* hash) {
    publish_t* res = malloc(sizeof(publish_t));
    res->filename = malloc(strlen(filename)+1);
    strcpy(res->filename, filename);
    res->extension = malloc(strlen(extension)+1);
    strcpy(res->extension, extension);
    res->keywords = malloc(keywords_size*sizeof(char*));
    for (unsigned int i=0; i<keywords_size; i++) {
        res->keywords[i] = malloc(strlen(keywords[i])+1);
        strcpy(res->keywords[i], keywords[i]);
    }
    res->keywords_size = keywords_size;
    res->hash = malloc(strlen(hash)+1);
    strcpy(res->hash, hash);
    return res;
}

search_t* new_search(const char** keywords, const size_t keywords_size) {
    search_t* res = malloc(sizeof(search_t));
    res->keywords = malloc(keywords_size*sizeof(char*));
    for (unsigned int i=0; i<keywords_size; i++) {
        res->keywords[i] = malloc(strlen(keywords[i])+1);
        strcpy(res->keywords[i], keywords[i]);
    }
    res->keywords_size = keywords_size;
    return res;
}

get_t* new_get(const char* filename, const char* extension, const char* hash) {
    get_t* res = malloc(sizeof(get_t));
    res->filename = malloc(strlen(filename)+1);
    strcpy(res->filename, filename);
    res->extension = malloc(strlen(extension)+1);
    strcpy(res->extension, extension);
    res->hash = malloc(strlen(hash)+1);
    strcpy(res->hash, hash);
    return res;
}

void* free_get(get_t* get) {
    free(get->filename);
    free(get->extension);
    free(get->hash);
    free(get);
    return NULL;
}