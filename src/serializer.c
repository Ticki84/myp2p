#include "serializer.h"
#include <stdio.h>
#include <math.h>
#include <malloc.h>

unsigned char* serialize_message(unsigned char* buffer, const message_type type, const void* message) {
    memset(buffer, 0, strlen((char*) buffer));
    buffer[0] = 0x8f;
    buffer[1] = 0x4;
    buffer[2] = 0x14;
    buffer[3] = 0x67;
    buffer[4] = type;
    switch (type) {
        case PUBLISH:
            return serialize_publish(buffer+5, message);
        case PUBLISH_ACK:
            return buffer+5;
        case PUBLISH_NACK:
            return serialize_nack(buffer+5, message);
        case SEARCH:
            return serialize_search(buffer+5, message);
        case SEARCH_RESP:
            return serialize_search_resp(buffer+5, message);
        case SEARCH_NACK:
            return serialize_nack(buffer+5, message);
        case GET:
            return serialize_get(buffer+5, message);
        case GET_ACK:
            return buffer+5;
        case GET_RESP:
            return serialize_get_resp(buffer+5, message);
        case GET_NACK:
            return serialize_nack(buffer+5, message);
        default:
            return NULL;
    }
}

unsigned char* serialize_bool(unsigned char* buffer, const int value) {
    buffer[0] = value != 0;
    return buffer + 1;
}

unsigned char* serialize_unsigned_short(unsigned char* buffer, const unsigned short value) {
    // value: 16 bits
    buffer[0] = value >> 8;
    buffer[1] = value;
    return buffer + 2;
}

unsigned char* serialize_unsigned_int(unsigned char* buffer, const unsigned int value) {
    // value: 32 bits
    buffer[0] = value >> 24;
    buffer[1] = value >> 16;
    buffer[2] = value >> 8;
    buffer[3] = value;
    return buffer + 4;
}

unsigned char* serialize_unsigned_str(unsigned char* buffer, const unsigned char* value) {
    size_t length = strlen((char*)value) + 1;
    if (length >> 32 > 0) fprintf(stderr, "Cannot serialize %s because it is too long, %zu bits needed (max. %.0f)", value, length, pow(2,32)-1);
    // length: 32 bits
    buffer[0] = length >> 24;
    buffer[1] = length >> 16;
    buffer[2] = length >> 8;
    buffer[3] = length;
    // value: length bits
    memcpy(buffer+4, value, length);
    return buffer + 4 + length;
}

unsigned char* serialize_str(unsigned char* buffer, const char* value) {
    size_t length = strlen(value) + 1;
    if (length >> 32 > 0) fprintf(stderr, "Cannot serialize %s because it is too long, %zu bits needed (max. %.0f)", value, length, pow(2,32)-1);
    // length: 32 bits
    buffer[0] = length >> 24;
    buffer[1] = length >> 16;
    buffer[2] = length >> 8;
    buffer[3] = length;
    // value: length bits
    strncpy((char*)buffer+4, value, length);
    return buffer + 4 + length;
}

unsigned char* serialize_str_arr(unsigned char* buffer, const size_t arr_size, const char** value) {
    if (arr_size >> 16 > 0) fprintf(stderr, "Cannot serialize array because it is too long, %zu bits needed (max. %.0f)", arr_size, pow(2,16)-1);
    // arr_size: 16 bits
    buffer[0] = arr_size >> 8;
    buffer[1] = arr_size;
    buffer += 2;
    for (unsigned int i=0; i<arr_size; i++) {
        buffer = serialize_str(buffer, value[i]);
    }
    return buffer;
}

unsigned char* serialize_publish(unsigned char* buffer, const publish_t* value) {
    /**
     * char* filename
     * char* extension
     * char** keywords
     * char* hash
     */
    buffer = serialize_str(buffer, value->filename);
    buffer = serialize_str(buffer, value->extension);
    buffer = serialize_str_arr(buffer, value->keywords_size, (const char **) value->keywords);
    buffer = serialize_str(buffer, value->hash);
    return buffer;
}

unsigned char* serialize_search(unsigned char* buffer, const search_t* value) {
    /**
     * char** keywords
     * size_t keywords_size
     */
    buffer = serialize_str_arr(buffer, value->keywords_size, (const char **) value->keywords);
    return buffer;
}

unsigned char* serialize_search_res(unsigned char* buffer, const search_res_t* value) {
    /**
     * char* ip_address
     * int port
     * char* filename
     * char* extension
     * char** keywords
     * size_t keywords_size
     * char* hash
     */
    buffer = serialize_str(buffer, value->ip_address);
    buffer = serialize_unsigned_short(buffer, value->port);
    buffer = serialize_str(buffer, value->filename);
    buffer = serialize_str(buffer, value->extension);
    buffer = serialize_str_arr(buffer, value->keywords_size, (const char **) value->keywords);
    buffer = serialize_str(buffer, value->hash);
    return buffer;
}

unsigned char* serialize_search_resp(unsigned char* buffer, const search_resp_t* value) {
    /**
     * search_res_t** results
     * size_t results_size
     */
    if (value->results_size >> 16 > 0) fprintf(stderr, "Cannot serialize array because it is too long, %zu bits needed (max. %.0f)", value->results_size, pow(2,16)-1);
    // value->results_size: 16 bits
    buffer[0] = value->results_size >> 8;
    buffer[1] = value->results_size;
    buffer += 2;
    for (unsigned int i=0; i<value->results_size; i++) {
        buffer = serialize_search_res(buffer, value->results[i]);
    }
    return buffer;
}

unsigned char* serialize_get(unsigned char* buffer, const get_t* value) {
    /**
     * char* filename
     * char* extension
     * char* hash
     */
    buffer = serialize_str(buffer, value->filename);
    buffer = serialize_str(buffer, value->extension);
    buffer = serialize_str(buffer, value->hash);
    return buffer;
}

unsigned char* serialize_get_resp(unsigned char* buffer, const get_resp_t* value) {
    /**
     * unsigned int file_size
     */
    buffer = serialize_unsigned_int(buffer, value->file_size);
    return buffer;
}

unsigned char* serialize_nack(unsigned char* buffer, const nack_t* value) {
    /**
     * char* error
     */
    buffer = serialize_str(buffer, value->error);
    return buffer;
}




unsigned char* deserialize_message(unsigned char* buffer, message_type* type) {
    if (buffer[0] != 0x8f || buffer[1] != 0x4 || buffer[2] != 0x14 || buffer[3] != 0x67) return NULL;
    *type = buffer[4];
    return buffer+5;
}

unsigned char* deserialize_bool(unsigned char* buffer, int* value) {
    *value = buffer[0];
    return buffer + 1;
}

unsigned char* deserialize_unsigned_short(unsigned char* buffer, unsigned short* value) {
    *value = (buffer[0] << 8) + buffer[1];
    return buffer + 2;
}

unsigned char* deserialize_unsigned_int(unsigned char* buffer, unsigned int* value) {
    *value = (buffer[0] << 24) + (buffer[1] << 16) + (buffer[2] << 8) + buffer[3];
    return buffer + 4;
}

unsigned char* deserialize_unsigned_str(unsigned char* buffer, unsigned char** value) {
    size_t length = (buffer[0] << 24) + (buffer[1] << 16) + (buffer[2] << 8) + buffer[3];
    *value = malloc(length*sizeof(unsigned char));
    memcpy(*value, buffer+4, length);
    return buffer + 4 + length;
}

unsigned char* deserialize_str(unsigned char* buffer, char** value) {
    size_t length = (buffer[0] << 24) + (buffer[1] << 16) + (buffer[2] << 8) + buffer[3];
    *value = malloc(length*sizeof(char));
    strncpy(*value, (char *) buffer+4, length);
    return buffer + 4 + length;
}

unsigned char* deserialize_str_arr(unsigned char* buffer, size_t* arr_size, char*** value) {
    *arr_size = (buffer[0] << 8) + buffer[1];
    buffer += 2;
    *value = malloc(*arr_size*sizeof(char*));
    for (unsigned int i=0; i<*arr_size; i++) {
        buffer = deserialize_str(buffer, &(*value)[i]);
    }
    return buffer;
}

unsigned char* deserialize_publish(unsigned char* buffer, publish_t* value) {
    /**
     * char* filename
     * char* extension
     * char** keywords
     * char* hash
     */
    buffer = deserialize_str(buffer, &value->filename);
    buffer = deserialize_str(buffer, &value->extension);
    buffer = deserialize_str_arr(buffer, &value->keywords_size, &value->keywords);
    buffer = deserialize_str(buffer, &value->hash);
    return buffer;
}

unsigned char* deserialize_search(unsigned char* buffer, search_t* value) {
    /**
     * char** keywords
     * size_t keywords_size
     */
    buffer = deserialize_str_arr(buffer, &value->keywords_size, &value->keywords);
    return buffer;
}

unsigned char* deserialize_search_res(unsigned char* buffer, search_res_t* value) {
    /**
     * char* ip_address
     * int port
     * char* filename
     * char* extension
     * char** keywords
     * size_t keywords_size
     * char* hash
     */
    buffer = deserialize_str(buffer, &value->ip_address);
    buffer = deserialize_unsigned_short(buffer, &value->port);
    buffer = deserialize_str(buffer, &value->filename);
    buffer = deserialize_str(buffer, &value->extension);
    buffer = deserialize_str_arr(buffer, &value->keywords_size, &value->keywords);
    buffer = deserialize_str(buffer, &value->hash);
    return buffer;
}

unsigned char* deserialize_search_resp(unsigned char* buffer, search_resp_t* value) {
    /**
     * search_res_t** results
     * size_t results_size
     */
    value->results_size = (buffer[0] << 8) + buffer[1];
    buffer += 2;
    value->results = malloc(value->results_size*sizeof(search_res_t*));
    for (unsigned int i=0; i<value->results_size; i++) {
        value->results[i] = malloc(sizeof(search_res_t));
        buffer = deserialize_search_res(buffer, &(*value->results[i]));
    }
    return buffer;
}

unsigned char* deserialize_get(unsigned char* buffer, get_t* value) {
    /**
     * char* filename
     * char* extension
     * char* hash
     */
    buffer = deserialize_str(buffer, &value->filename);
    buffer = deserialize_str(buffer, &value->extension);
    buffer = deserialize_str(buffer, &value->hash);
    return buffer;
}

unsigned char* deserialize_get_resp(unsigned char* buffer, get_resp_t* value) {
    /**
     * unsigned int file_size
     */
    buffer = deserialize_unsigned_int(buffer, &value->file_size);
    return buffer;
}

unsigned char* deserialize_nack(unsigned char* buffer, nack_t* value) {
    /**
     * char* error
     */
    buffer = deserialize_str(buffer, &value->error);
    return buffer;
}